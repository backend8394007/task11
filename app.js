//Создадим массив строк(string)
let arrayOfNames = ['Вася', 'Петя', 'Иван'];
console.log('Массив строк:');
console.log(arrayOfNames);

//Создадим массив чисел(number)
let arrayOfNumber = [11, 23, 37 , 43, 59];
console.log('Массив чисел:');
console.log(arrayOfNumber);

//Создадим объект
let object =
    {
    name: 'Eugene',
        age: 19
    };
console.log('Объект:');
console.log(object);
console.log();

//Создадим массив объектов
let personalData = [
    {name: 'Вася', age: 11,},
    {name: 'Иван', age: 23,},
    {name: 'Петя', age: 37},
];
console.log('Массив объектов:');
console.log(personalData);

//Найдем первый попавшийся объект с элементом имени 'Петя'
let objectName = personalData.find(function (item){
   return item.name === 'Петя';
});
console.log('Первый попавшийся объект с элементом имени \'Петя\':');
console.log(objectName);
console.log();

//Добавим в конец новый объект
personalData.push({name: 'Костя', age: 37});

//Найдем все объекты с возрастом 37
let objectAge = personalData.filter((item) => item.age === 37);
console.log('Все объекты с возрастом 37:');
console.log(objectAge);
console.log();

//Отсортируем массив объектов по возрасту в обратном порядке
personalData.sort((item1,  item2) => item2.age - item1.age);
console.log('Массив объектов по возрасту в обратном порядке:');
console.log(personalData);

//Изменим элемент объекта
personalData.map((item) => {
    if (item.name === 'Иван') item.age = 24;
});
console.log(`Измененный возраст: ${personalData[2].age}`);

